import React, { useState } from 'react';
import './App.css';
import Barcode from 'react-barcode';
import { Button, Form, type FormProps, Input, Row, Col } from 'antd';

type FieldType = {
  barcodeValue?: string;
};

function App() {

  const [barcodeValue, setBarcodeValue] = useState<string | null>(null);

  const onFinish: FormProps<FieldType>["onFinish"] = (values) => {
    if (values.barcodeValue !== undefined) {
      setBarcodeValue(values.barcodeValue)
    }
  };


  return (
    <Row>
      <Col xs={24} sm={24} md={18} lg={16} xl={12}>
        <h1>Generate barcode code-128</h1>
        <div className="barcode-container">
        {barcodeValue === null ? <></> : <Barcode width={2} height={200} value={barcodeValue} />}
        </div>
        <Form
          name="basic"
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item<FieldType>
            name="barcodeValue"
            rules={[{ required: true, message: 'Required!' }]}
          >
            <Input size="large" placeholder='Enter your barcode here' />
          </Form.Item>
          <Form.Item>
            <Button className='button-generate' size="large" type="primary" htmlType="submit">
              Generate
            </Button>
          </Form.Item>
        </Form>
        
      </Col>
    </Row>
  );
}

export default App;
