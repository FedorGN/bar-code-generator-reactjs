# Generate barcode code-128 application

Simple one page ReactJS application to generate barcode code-128.

Application is deployed on Vercel: https://bar-code-generator-reactjs.vercel.app/